# Build

After cloning the repository, you need to initialize the project by doing:

    make init

This will link several files necessary for building. Then, if you haven't cloned
with the recursive flag to retrieve submodules, do so now:

    git submodule update --init --recursive

Before continuing, be sure to have installed the following package:

    sudo apt install kconfig-frontends

Then, configure the project for the board:

    make configure BOARDCONFIG=nucleo-l476rg:nsh

And copy the customized configuration for bicycle companion:

    cp configs/nucleo-memlcd nuttx/.config

Finally, you can build the project with

    make

# Flashing

TODO

# Debugging

TODO

# Power Management


Sleep modes:
- Riding: PWM + Pulse Counter -> Stop 1 (need both LPTIM working)
- Not Riding: PWM -> Stop 2 (wakeup via reed and/or acc and/or button)
- Auto shutdown: Standby/Shutdown?

Current usage:
- lowpower config: stop1 -> 7uA, stop2 -> 2uA, run (UART2 + TIM2+5 only + MSI 4MHz, no PLLs, Range2) 736uA
- as above but enabling SPI1 + I2C1 + LPTIM1 + LPTIM2: 880uA 

LCD: 3uA when not updating

# License

TBD

# Attribution

Basic World Cities Database: The Provider offers a Basic World Cities Database free of charge. This database is licensed under the Creative Commons Attribution 4.0 license as described at: https://creativecommons.org/licenses/by/4.0/

https://simplemaps.com/static/data/world-cities/basic/simplemaps_worldcities_basicv1.6.zip
