/****************************************************************************
 * drivers/sensors/mc6470_acc.c
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <stdbool.h>
#include <string.h>
#include <poll.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/kmalloc.h>
#include <nuttx/signal.h>
#include <nuttx/fs/fs.h>
#include <nuttx/i2c/i2c_master.h>
#include <sys/ioctl.h>
#include <nuttx/sensors/ioctl.h>

#include <nuttx/irq.h>

#include "mc6470_common.h"
#include "mc6470_acc.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define MC6470_ACC_ADDR1    0x4C
#define MC6470_ACC_ADDR2    0x6C

/* Accelerometer register definitions */

#define MC6470_ACC_INTEN                0x06

#define MC6470_ACC_XOUT_L               0x0D
#define MC6470_ACC_XOUT_H               0x0E
#define MC6470_ACC_YOUT_L               0x0F
#define MC6470_ACC_YOUT_H               0x10
#define MC6470_ACC_ZOUT_L               0x11
#define MC6470_ACC_ZOUT_H               0x12

#define MC6470_ACC_SR                   0x03
#define MC6470_ACC_OPSTAT               0x04
#define MC6470_ACC_MODE                 0x07
#define MC6470_ACC_SRTFR                0x08
#define MC6470_ACC_TAPEN                0x09

#define MC6470_ACC_TTTRX                0x0A
#define MC6470_ACC_TTTRY                0x0B
#define MC6470_ACC_TTTRZ                0x0C

#define MC6470_ACC_OUTCFG               0x20

#define MC6470_ACC_SR_TAP_MASK          (0b111111)
#define MC6470_ACC_SR_TAP_XP            (1 << 0)
#define MC6470_ACC_SR_TAP_XN            (1 << 1)
#define MC6470_ACC_SR_TAP_YP            (1 << 2)
#define MC6470_ACC_SR_TAP_YN            (1 << 3)
#define MC6470_ACC_SR_TAP_ZP            (1 << 4)
#define MC6470_ACC_SR_TAP_ZN            (1 << 5)
#define MC6470_ACC_SR_ACQ_INT           (1 << 7)

#define MC6470_ACC_INTEN_TIEN_MASK      (0b111111)
#define MC6470_ACC_INTEN_TIXPEN         (1 << 0)
#define MC6470_ACC_INTEN_TIXNEN         (1 << 1)
#define MC6470_ACC_INTEN_TIYPEN         (1 << 2)
#define MC6470_ACC_INTEN_TIYNEN         (1 << 3)
#define MC6470_ACC_INTEN_TIZPEN         (1 << 4)
#define MC6470_ACC_INTEN_TIZNEN         (1 << 5)
#define MC6470_ACC_INTEN_ACQ_INT_EN     (1 << 7)

#define MC6470_ACC_TAPEN_TAPXPEN        (1 << 0)
#define MC6470_ACC_TAPEN_TAPXNEN        (1 << 1)
#define MC6470_ACC_TAPEN_TAPYPEN        (1 << 2)
#define MC6470_ACC_TAPEN_TAPYNEN        (1 << 3)
#define MC6470_ACC_TAPEN_TAPZPEN        (1 << 4)
#define MC6470_ACC_TAPEN_TAPZNEN        (1 << 5)
#define MC6470_ACC_TAPEN_THRDUR         (1 << 6)
#define MC6470_ACC_TAPEN_ENABLE         (1 << 7)

#define MC6470_ACC_SRTFR_RATE_MASK      0b1111
#define MC6470_ACC_SRTFR_RATE_32HZ      0
#define MC6470_ACC_SRTFR_RATE_16HZ      1
#define MC6470_ACC_SRTFR_RATE_8HZ       2
#define MC6470_ACC_SRTFR_RATE_4HZ       3
#define MC6470_ACC_SRTFR_RATE_2HZ       4
#define MC6470_ACC_SRTFR_RATE_1HZ       5
#define MC6470_ACC_SRTFR_RATE_05HZ      6
#define MC6470_ACC_SRTFR_RATE_025HZ     7
#define MC6470_ACC_SRTFR_RATE_64HZ      8
#define MC6470_ACC_SRTFR_RATE_128HZ     9
#define MC6470_ACC_SRTFR_RATE_256HZ     10

#define MC6470_ACC_OPSTAT_STAT_SHIFT    0
#define MC6470_ACC_OPSTAT_STAT_STANDBY  (0 << MC6470_ACC_OPSTAT_STAT_SHIFT)
#define MC6470_ACC_OPSTAT_STAT_WAKE     (1 << MC6470_ACC_OPSTAT_STAT_SHIFT)

#define MC6470_ACC_MODE_WAKE            (1 << 0)
#define MC6470_ACC_MODE_IPP             (1 << 6)
#define MC6470_ACC_MODE_IAH             (1 << 7)

#define MC6470_ACC_OUTCFG_RES_MASK      0b111
#define MC6470_ACC_OUTCFG_RES6          0b000
#define MC6470_ACC_OUTCFG_RES7          0b001
#define MC6470_ACC_OUTCFG_RES8          0b010
#define MC6470_ACC_OUTCFG_RES10         0b011
#define MC6470_ACC_OUTCFG_RES12         0b100
#define MC6470_ACC_OUTCFG_RES14         0b101

#define MC6470_ACC_OUTCFG_RANGE_SHIFT   4
#define MC6470_ACC_OUTCFG_RANGE_MASK    (0b111 << MC6470_ACC_OUTCFG_RANGE_SHIFT)
#define MC6470_ACC_OUTCFG_RANGE_2G      (0b000 << MC6470_ACC_OUTCFG_RANGE_SHIFT)
#define MC6470_ACC_OUTCFG_RANGE_4G      (0b001 << MC6470_ACC_OUTCFG_RANGE_SHIFT)
#define MC6470_ACC_OUTCFG_RANGE_8G      (0b010 << MC6470_ACC_OUTCFG_RANGE_SHIFT)
#define MC6470_ACC_OUTCFG_RANGE_16G     (0b011 << MC6470_ACC_OUTCFG_RANGE_SHIFT)

/****************************************************************************
 * Private Types
 ****************************************************************************/

/* This structure provides the state of the mc6470_acc driver */

struct mc6470_acc_dev_s
{
  struct i2c_master_s* i2c;
  struct mc6470_acc_lower_dev_s* lower;

  uint8_t devaddr;
  bool initialized;
  sem_t exclsem, datasem;
  int open_count;
  bool acc_state;    /* enabled (continuous measurement) or not (standby) */
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* Character driver methods */

static int     mc6470_acc_open(FAR struct file *filep);
static int     mc6470_acc_close(FAR struct file *filep);
static int     mc6470_acc_ioctl(FAR struct file *filep, int cmd, unsigned long arg);
static ssize_t mc6470_acc_read(FAR struct file *filep, char* buffer, size_t size);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations mc6470_acc_fops =
{
    mc6470_acc_open,  /* open */
    mc6470_acc_close, /* close */
    mc6470_acc_read,  /* read */
    NULL,             /* write */
    NULL,             /* seek */
    mc6470_acc_ioctl, /* ioctl */
    NULL              /* poll */
#ifndef CONFIG_DISABLE_PSEUDOFS_OPERATIONS
    , NULL            /* unlink */
#endif
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

static int acc_setstate(struct mc6470_acc_dev_s* dev, bool wake)
{
  int ret;

  /* set WAKE bit */

  ret = mc6470_modifyreg8(dev->i2c, dev->devaddr, MC6470_ACC_MODE,
                          (wake ? 0 : MC6470_ACC_MODE_WAKE),
                          (wake ? MC6470_ACC_MODE_WAKE : 0));

  if (ret < 0)
    {
      return ret;
    }
  else
    {
      dev->acc_state = wake;
      return 0;
    }
}

static int acc_status(struct mc6470_acc_dev_s* dev, uint8_t* status)
{
  irqstate_t flags;
  int ret;

  flags = enter_critical_section();

  ret = mc6470_readreg8(dev->i2c, dev->devaddr, MC6470_ACC_SR, status, 1);

  leave_critical_section(flags);

  return ret;
}

static int acc_read_data(struct mc6470_acc_dev_s* dev, int16_t* data)
{
  int ret;
  uint8_t regvals[6];

  ret = mc6470_readreg8(dev->i2c, dev->devaddr, MC6470_ACC_XOUT_L,
                        regvals, 6);

  data[0] = regvals[0] | (regvals[1] << 8);
  data[1] = regvals[2] | (regvals[3] << 8);
  data[2] = regvals[4] | (regvals[5] << 8);

  return ret;
}

static int acc_configure(struct mc6470_acc_dev_s* dev)
{
  int ret;
  uint8_t regval;

  /* Determine which I2C address was assigned (errata for REV1 board,
   * A5 was left unconnected) */

  {
    uint8_t dummy;

    dev->devaddr = MC6470_ACC_ADDR1;
    ret = acc_status(dev, &dummy);

    if (ret < 0)
      {
        dev->devaddr = MC6470_ACC_ADDR2;
        ret = acc_status(dev, &dummy);

        if (ret < 0)
          {
            return ret;
          }
      }
  }

  /* Ensure STANDBY mode (needed for writing registers) */

  ret = acc_setstate(dev, false);

  /* Set maximum range and resolution */

  regval = (MC6470_ACC_OUTCFG_RES14 | MC6470_ACC_OUTCFG_RANGE_16G);
  ret = mc6470_modifyreg8(dev->i2c, dev->devaddr, MC6470_ACC_OUTCFG,
                          MC6470_ACC_OUTCFG_RES_MASK |
                          MC6470_ACC_OUTCFG_RANGE_MASK, regval);

  /* Set maximum rate */

  ret = mc6470_modifyreg8(dev->i2c, dev->devaddr, MC6470_ACC_SRTFR,
                          MC6470_ACC_SRTFR_RATE_MASK,
                          MC6470_ACC_SRTFR_RATE_256HZ);

  /* Ensure open-drain, active-low interrupts */

  ret = mc6470_modifyreg8(dev->i2c, dev->devaddr, MC6470_ACC_MODE,
                          MC6470_ACC_MODE_IAH | MC6470_ACC_MODE_IPP, 0);

#ifdef CONFIG_MC6470_ACC_INTERRUPTS
  /* Enable data ready and tap interrupts (taps may not be enabled) */

  ret = mc6470_modifyreg8(dev->i2c, dev->devaddr, MC6470_ACC_INTEN, 0,
                          MC6470_ACC_INTEN_TIEN_MASK |
                          MC6470_ACC_INTEN_ACQ_INT_EN);
#endif

  return ret;
}

static int acc_tapconfigure(struct mc6470_acc_dev_s* dev,
                            struct mc6470_acc_tap_settings_s* settings)
{
  int ret;
  ret = mc6470_modifyreg8(dev->i2c, dev->devaddr, MC6470_ACC_TAPEN,
                          (0xff >> 1), /* clear all but TAP_EN */
                          settings->tap_detect);

  if (ret >= 0)
    {
      if (settings->tap_detect & MC6470_ACC_TAPEN_THRDUR)
        {
          ret = mc6470_writereg8(dev->i2c, dev->devaddr, MC6470_ACC_TTTRX, settings->threshold[0]);
          ret = mc6470_writereg8(dev->i2c, dev->devaddr, MC6470_ACC_TTTRY, settings->threshold[1]);
          ret = mc6470_writereg8(dev->i2c, dev->devaddr, MC6470_ACC_TTTRZ, settings->threshold[2]);
        }
      else
        {
          ret = mc6470_writereg8(dev->i2c, dev->devaddr, MC6470_ACC_TTTRX, settings->duration[0] | (settings->quiet_time[0] << 4));
          ret = mc6470_writereg8(dev->i2c, dev->devaddr, MC6470_ACC_TTTRY, settings->duration[1] | (settings->quiet_time[1] << 4));
          ret = mc6470_writereg8(dev->i2c, dev->devaddr, MC6470_ACC_TTTRZ, settings->duration[2] | (settings->quiet_time[2] << 4));
        }
    }

  return ret;
}

static int acc_tapenable(struct mc6470_acc_dev_s* dev, bool enable)
{
  /*
   * Enable tap interrupts (even without interrupt processing this allows
   * to wakeup on taps
   */

  int ret = mc6470_modifyreg8(dev->i2c, dev->devaddr, MC6470_ACC_INTEN, 0,
                              MC6470_ACC_INTEN_TIEN_MASK);

  if (ret < 0)
  {
    return ret;
  }

  return mc6470_modifyreg8(dev->i2c, dev->devaddr, MC6470_ACC_TAPEN,
                           (enable ? 0 : MC6470_ACC_TAPEN_ENABLE),
                           (enable ? MC6470_ACC_TAPEN_ENABLE : 0));
}

#ifdef CONFIG_MC6470_ACC_INTERRUPTS

static int acc_interrupt(int irq, FAR void *context, FAR void *arg)
{
  irqstate_t flags;
  int sval;
  struct mc6470_acc_dev_s* priv = (struct mc6470_acc_dev_s*)arg;

  flags = enter_critical_section();

  /* first check if a read() is blocked */

  nxsem_getvalue(&priv->datasem, &sval);

  if (sval < 1)
    {
      /* unblock */

      nxsem_post(&priv->datasem);
    }

  leave_critical_section(flags);

  return OK;
}

#endif

/****************************************************************************
 * Name: mc6470_acc_open
 ****************************************************************************/

static int mc6470_acc_open(FAR struct file *filep)
{
  FAR struct mc6470_acc_dev_s *priv;
  int ret = OK;

  priv = (FAR struct mc6470_acc_dev_s *)filep->f_inode->i_private;

  nxsem_wait_uninterruptible(&priv->exclsem);

  /* Configure sensor if this is the first use */

  if (priv->open_count == 0)
    {
      ret = acc_configure(priv);
    }

  if (ret >= 0)
    {
      priv->open_count++;
    }

  nxsem_post(&priv->exclsem);

  return ret;
}

/****************************************************************************
 * Name: mc6470_acc_close
 ****************************************************************************/

static int mc6470_acc_close(FAR struct file *filep)
{
  FAR struct mc6470_acc_dev_s *priv;
  int ret = OK;

  priv  = (FAR struct mc6470_acc_dev_s *)filep->f_inode->i_private;

  nxsem_wait_uninterruptible(&priv->exclsem);

  priv->open_count--;

  if (priv->open_count == 0)
    {
      ret = acc_setstate(priv, false);
    }

  nxsem_post(&priv->exclsem);

  return ret;
}

/****************************************************************************
 * Name: mc6470_acc_read
 ****************************************************************************/

static ssize_t mc6470_acc_read(FAR struct file *filep, char* buffer,
                               size_t size)
{
  FAR struct mc6470_acc_dev_s *priv;
  int ret = OK;
  uint8_t status = 0;

  priv  = (FAR struct mc6470_acc_dev_s *)filep->f_inode->i_private;

  nxsem_wait_uninterruptible(&priv->exclsem);

  /* if accelerometer is not enabled (STANDBY), can't read data */

  if (!priv->acc_state)
    {
      ret = -ENODATA;
    }
  else
    {
      struct mc6470_acc_data_s* data = (struct mc6470_acc_data_s*)buffer;
      data->tap_state = 0;

      /* wait for new data to be available */

#ifndef CONFIG_MC6470_ACC_INTERRUPTS
      while (!(status & MC6470_ACC_SR_ACQ_INT) && ret >= 0)
        {
          ret = acc_status(priv, &status);
          data->tap_state |= status; /* so that tap bits are not lost */
        }
#else
#if 0
      /* clear bits to trigger intterupt */

      acc_status(priv, &status);
      data->tap_state |= status; /* so that tap bits are not lost */
#else
      while (!(status & MC6470_ACC_SR_ACQ_INT) && ret >= 0)
        {
          ret = acc_status(priv, &status);
          data->tap_state |= status; /* so that tap bits are not lost */
        }
#endif
      /* wait for interrupt */

      nxsem_wait_uninterruptible(&priv->datasem);

      /* read status (will clear bits) */

      acc_status(priv, &status);

      data->tap_state |= status;
#endif

      ret = acc_read_data(priv, data->acc);
    }

  nxsem_post(&priv->exclsem);

  if (ret >= 0)
    {
      ret = sizeof(struct mc6470_acc_data_s);
    }

  return ret;
}

/****************************************************************************
 * Name: mc6470_ioctl
 ****************************************************************************/

static int mc6470_acc_ioctl(FAR struct file *filep, int cmd,
                            unsigned long arg)
{
  FAR struct mc6470_acc_dev_s *priv;
  int ret = OK;

  priv  = (FAR struct mc6470_acc_dev_s *)filep->f_inode->i_private;

  nxsem_wait_uninterruptible(&priv->exclsem);

  switch(cmd)
  {
    case SNIOC_ENABLE:
      ret = acc_setstate(priv, true);
    break;
    case SNIOC_DISABLE:
      ret = acc_setstate(priv, false);
    break;
    case SNIOC_TAP_DETECT:
      {
        if (priv->acc_state)
          {
            /* need to disable accelerometer to change settings */

            ret = -EACCES;
          }
        else
          {
            ret = acc_tapenable(priv, (bool)arg);
          }
      }
    break;
    case SNIOC_TAP_CONFIGURE:
      {
        if (priv->acc_state)
          {
            /* need to disable accelerometer to change settings */

            ret = -EACCES;
          }
        else
          {
            ret = acc_tapconfigure(priv, (struct mc6470_acc_tap_settings_s*)arg);
          }
      }
    break;
    default:
      ret = -EINVAL;
    break;
  }

  nxsem_post(&priv->exclsem);

  return ret;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: mc6470_register
 *
 * Description:
 *   Register the mc6470 driver as the specified device.
 *
 * Input Parameters:
 *   devname - The name of the mc6470 device to be registered.
 *
 * Returned Value:
 *   Zero (OK) is returned on success.  Otherwise a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 ****************************************************************************/

int mc6470_acc_register(FAR const char *devname, struct i2c_master_s* i2c,
                        struct mc6470_acc_lower_dev_s* lower)
{
  FAR struct mc6470_acc_dev_s *priv;
  int ret = OK;

  DEBUGASSERT(devname);
  DEBUGASSERT(lower);

  /* Allocate a new mc6470 driver instance */

  priv = (FAR struct mc6470_acc_dev_s *)kmm_zalloc(sizeof(struct mc6470_acc_dev_s));

  priv->i2c = i2c;
  priv->lower = lower;
  nxsem_init(&priv->exclsem, 0, 1);
  nxsem_init(&priv->datasem, 0, 0);

#ifdef CONFIG_MC6470_ACC_INTERRUPTS
  priv->lower->enable_irq(acc_interrupt, priv);
#endif

  ret = register_driver(devname, &mc6470_acc_fops, 0666, priv);

  if (ret < 0)
  {
    kmm_free(priv);
    ret = ERROR;
  }

  return ret;
}
