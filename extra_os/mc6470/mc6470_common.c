/****************************************************************************
 * mc6470/mc6470.c
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <debug.h>
#include <nuttx/irq.h>

#include "mc6470_acc.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

int mc6470_readreg8(FAR struct i2c_master_s *i2c, uint8_t devaddr,
                    uint8_t regaddr, FAR uint8_t * regval, size_t size)
{
  struct i2c_config_s config;
  int ret;

  /* Sanity check */

  DEBUGASSERT(i2c != NULL);
  DEBUGASSERT(regval != NULL);

  /* Set up the I2C configuration */

  config.frequency = CONFIG_MC6470_I2C_FREQUENCY;
  config.address = devaddr;
  config.addrlen = 7;

  /* Write the register address and read back the data */

  ret = i2c_writeread(i2c, &config, &regaddr, sizeof(regaddr), regval, size);
  if (ret < 0)
    {
      snerr("ERROR: i2c_writeread failed: %d\n", ret);
      return ret;
    }

  return OK;
}

int mc6470_writereg8(FAR struct i2c_master_s *i2c, uint8_t devaddr,
                     uint8_t regaddr, uint8_t regval)
{
  struct i2c_config_s config;
  uint8_t buffer[2];
  int ret;

  /* Sanity check */

  DEBUGASSERT(i2c != NULL);

  /* Set up a 2-byte message to send */

  buffer[0] = regaddr;
  buffer[1] = regval;

  /* Set up the I2C configuration */

  config.frequency = CONFIG_MC6470_I2C_FREQUENCY;
  config.address = devaddr;
  config.addrlen = 7;

  /* Write the register address followed by the data (no RESTART) */

  ret = i2c_write(i2c, &config, buffer, sizeof(buffer));
  if (ret < 0)
    {
      snerr("ERROR: i2c_write failed: %d\n", ret);
      return ret;
    }

  sninfo("addr: %02x value: %02x\n", regaddr, regval);
  return OK;
}

int mc6470_modifyreg8(FAR struct i2c_master_s *i2c,
                      uint8_t devaddr, uint8_t regaddr,
                      uint8_t clearbits, uint8_t setbits)
{
  int ret;
  uint8_t regval;

  /* Sanity check */

  DEBUGASSERT(i2c != NULL);

  ret = mc6470_readreg8(i2c, devaddr, regaddr, &regval, 1);
  if (ret < 0)
    {
      snerr("ERROR: mc6470_readreg8 failed: %d\n", ret);
      return ret;
    }

  regval &= ~clearbits;
  regval |= setbits;

  ret = mc6470_writereg8(i2c, devaddr, regaddr, regval);
  if (ret < 0)
    {
      snerr("ERROR: mc6470_writereg8 failed: %d\n", ret);
      return ret;
    }

  return OK;
}
