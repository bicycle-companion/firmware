/****************************************************************************
 * drivers/sensors/mc6470_acc.h
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

#ifndef __NUTTX_SENSORS_MC6470_ACC_H__
#define __NUTTX_SENSORS_MC6470_ACC_H__

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/sensors/ioctl.h>
#include <nuttx/i2c/i2c_master.h>
#include <nuttx/irq.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* IOCTL codes */

#define SNIOC_TAP_DETECT                _SNIOC(0x0001) /* Arg: bool: true = enable */
#define SNIOC_TAP_CONFIGURE             _SNIOC(0x0002) /* Arg: mc6470_acc_tap_settings_s* */

#define MC6470_TAP_X_POS                (1 << 0)
#define MC6470_TAP_X_NEG                (1 << 1)
#define MC6470_TAP_Y_POS                (1 << 2)
#define MC6470_TAP_Y_NEG                (1 << 3)
#define MC6470_TAP_Z_POS                (1 << 4)
#define MC6470_TAP_Z_NEG                (1 << 5)

#define MC6470_TAP_BY_THRESHOLD         (1 << 6)

/****************************************************************************
 * Public Types
 ****************************************************************************/

struct mc6470_acc_data_s
{
  int16_t acc[3];
  uint8_t tap_state;
};

struct mc6470_acc_tap_settings_s
{
  uint8_t tap_detect;

  uint8_t quiet_time[3], duration[3]; /* 4 bits each, in units of samples */
  uint8_t threshold[3]; /* 8 bits, raw value */
};

struct mc6470_acc_lower_dev_s
{
  void (*enable_irq)(xcpt_t irqhandler, void* arg);
};

/****************************************************************************
 * Public Data
 ****************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Name: mc6470_acc_register
 *
 * Description:
 *   Register the mc6470 character driver as the specified device.
 *
 * Input Parameters:
 *   devname - The name of the mc6470 device to be registered
 *   i2c     - The I2C bus instance
 *
 * Returned Value:
 *   Zero (OK) is returned on success.  Otherwise a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 ****************************************************************************/

int mc6470_acc_register(FAR const char *devname, struct i2c_master_s* i2c,
                        struct mc6470_acc_lower_dev_s* lower);

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif // __NUTTX_SENSORS_MC6470_ACC_H__
