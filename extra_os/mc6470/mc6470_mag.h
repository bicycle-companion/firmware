/****************************************************************************
 * drivers/sensors/mc6470.h
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

#ifndef __NUTTX_SENSORS_MC6470_H__
#define __NUTTX_SENSORS_MC6470_H__

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/sensors/ioctl.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* IOCTL codes */

#define SNIOC_SETMODE                   _SNIOC(0x0001) /* Arg: bool (true=continous) */
#define SNIOC_SETOFFSET                 _SNIOC(0x0002) /* Arg: int16_t[3] */
#define SNIOC_GETOFFSET                 _SNIOC(0x0003) /* Arg: int16_t[3] */

/****************************************************************************
 * Public Types
 ****************************************************************************/

struct mc6470_mag_data_s
{
  int16_t mag[3];
};

/****************************************************************************
 * Public Data
 ****************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Name: mc6470_mag_register
 *
 * Description:
 *   Register the mc6470 character driver as the specified device.
 *
 * Input Parameters:
 *   devname - The name of the mc6470 device to be registered
 *   i2c     - The I2C bus instance
 *
 * Returned Value:
 *   Zero (OK) is returned on success.  Otherwise a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 ****************************************************************************/

int mc6470_mag_register(FAR const char *devname, struct i2c_master_s* i2c);

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif // __NUTTX_SENSORS_MC6470_H__
