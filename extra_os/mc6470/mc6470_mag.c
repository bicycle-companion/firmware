/****************************************************************************
 * drivers/sensors/mc6470_mag.c
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <stdbool.h>
#include <string.h>
#include <poll.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/kmalloc.h>
#include <nuttx/signal.h>
#include <nuttx/fs/fs.h>
#include <nuttx/i2c/i2c_master.h>
#include <sys/ioctl.h>
#include <nuttx/sensors/ioctl.h>

#include <nuttx/irq.h>

#include "mc6470_common.h"
#include "mc6470_mag.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define MC6470_MAG_ADDR                 0x0C

/* Magnetometer register definitions */

#define MC6470_MAG_XOUT_L               0x10
#define MC6470_MAG_XOUT_H               0x11
#define MC6470_MAG_YOUT_L               0x12
#define MC6470_MAG_YOUT_H               0x13
#define MC6470_MAG_ZOUT_L               0x14
#define MC6470_MAG_ZOUT_H               0x15

#define MC6470_MAG_STAT                 0x18

#define MC6470_MAG_OFFX_L               0x20
#define MC6470_MAG_OFFX_H               0x21
#define MC6470_MAG_OFFY_L               0x22
#define MC6470_MAG_OFFY_H               0x23
#define MC6470_MAG_OFFZ_L               0x24
#define MC6470_MAG_OFFZ_H               0x25

#define MC6470_MAG_CTRL1                0x1B
#define MC6470_MAG_CTRL3                0x1D
#define MC6470_MAG_CTRL4                0x1E

#define MC6470_MAG_TEMP                 0x31

#define MC6470_MAG_STAT_DRDY            (1 << 6)

#define MC6470_MAG_CTRL1_FS             (1 << 1)

#define MC6470_MAG_CTRL1_ODR_SHIFT      3
#define MC6470_MAG_CTRL1_ODR_MASK       (0b11 << MC6470_MAG_CTRL1_ODR_SHIFT)
#define MC6470_MAG_CTRL1_ODR_05         (0 << MC6470_MAG_CTRL1_ODR_SHIFT)
#define MC6470_MAG_CTRL1_ODR_10         (1 << MC6470_MAG_CTRL1_ODR_SHIFT)
#define MC6470_MAG_CTRL1_ODR_20         (2 << MC6470_MAG_CTRL1_ODR_SHIFT)
#define MC6470_MAG_CTRL1_ODR_100        (3 << MC6470_MAG_CTRL1_ODR_SHIFT)

#define MC6470_MAG_CTRL1_PC             (1 << 7)


#define MC6470_MAG_CTRL3_OCL            (1 << 0)
#define MC6470_MAG_CTRL3_TCS            (1 << 1)
#define MC6470_MAG_CTRL3_FRC            (1 << 6)
#define MC6470_MAG_CTRL3_SRST           (1 << 7)

#define MC6470_MAG_CTRL4_RS15           (1 << 4)

// TODO: does not seem to work
//#define MC6470_ENABLE_15BITRES

/****************************************************************************
 * Private Types
 ****************************************************************************/

/* This structure provides the state of the mc6470_acc driver */

struct mc6470_mag_dev_s
{
  struct i2c_master_s* i2c;

  sem_t exclsem;
  int open_count;
  bool mag_state;   /* power-down or active */
  bool continous;   /* continuous measurement or forced */
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* Character driver methods */

static int     mc6470_mag_open(FAR struct file *filep);
static int     mc6470_mag_close(FAR struct file *filep);
static int     mc6470_mag_ioctl(FAR struct file *filep, int cmd, unsigned long arg);
static ssize_t mc6470_mag_read(FAR struct file *filep, char* buffer, size_t size);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations mc6470_mag_fops =
{
    mc6470_mag_open,  /* open */
    mc6470_mag_close, /* close */
    mc6470_mag_read,  /* read */
    NULL,             /* write */
    NULL,             /* seek */
    mc6470_mag_ioctl, /* ioctl */
    NULL              /* poll */
#ifndef CONFIG_DISABLE_PSEUDOFS_OPERATIONS
    , NULL            /* unlink */
#endif
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

static int mag_setstate(struct mc6470_mag_dev_s* dev, bool active)
{
  int ret;
  ret = mc6470_modifyreg8(dev->i2c, MC6470_MAG_ADDR, MC6470_MAG_CTRL1,
                          (active ? 0 : MC6470_MAG_CTRL1_PC),
                          (active ? MC6470_MAG_CTRL1_PC : 0));

  if (ret >= 0)
    {
      dev->mag_state = active;
    }

  return ret;
}

static int mag_setmode(struct mc6470_mag_dev_s* dev, bool continuous)
{
  int ret;

  ret = mc6470_modifyreg8(dev->i2c, MC6470_MAG_ADDR, MC6470_MAG_CTRL1,
                          (continuous ? MC6470_MAG_CTRL1_FS : 0),
                          (continuous ? 0 : MC6470_MAG_CTRL1_FS));

  return ret;
}

static int mag_read_data(struct mc6470_mag_dev_s* dev, int16_t* data)
{
  int ret, i;

  ret = mc6470_readreg8(dev->i2c, MC6470_MAG_ADDR, MC6470_MAG_XOUT_L,
                        (uint8_t*)data, 6);

#ifdef MC6470_ENABLE_15BITRES
  /* extend sign from 15bit to 16bit */
  // TODO: 15bit not supported? datasheet inconsistent

  for (i = 0; i < 3; i++)
    {
      data[i] |= (data[i] & (1 << 14) ? 1 << 15 : 0);
    }
#else
  /* extend sign from 14bit to 16bit */

  for (i = 0; i < 3; i++)
    {
      // TODO: already extended? datasheet says otherwise
      //data[i] |= (data[i] & (1 << 13) ? 0b11 << 14 : 0);
    }
#endif

  return ret;
}

static int mag_status(struct mc6470_mag_dev_s* dev, uint8_t* status)
{
  return mc6470_readreg8(dev->i2c, MC6470_MAG_ADDR, MC6470_MAG_STAT,
                         status, 1);
}

static int mag_configure(struct mc6470_mag_dev_s* dev)
{
  int ret;

  /* Reset magnetometer (will go into standby, forced state) */

  ret = mc6470_modifyreg8(dev->i2c, MC6470_MAG_ADDR, MC6470_MAG_CTRL3, 0,
                          MC6470_MAG_CTRL3_SRST);

#ifdef MC6470_ENABLE_15BITRES
  /* Set full resolution */

  ret = mc6470_modifyreg8(dev->i2c, MC6470_MAG_ADDR, MC6470_MAG_CTRL4,
                          0, MC6470_MAG_CTRL4_RS15);
#endif

  /* Set to 10Hz ODR for continuous measurements */

  ret = mc6470_modifyreg8(dev->i2c, MC6470_MAG_ADDR, MC6470_MAG_CTRL1,
                          MC6470_MAG_CTRL1_ODR_MASK,
                          MC6470_MAG_CTRL1_ODR_10);

  return ret;
}

static int mag_setoffset(struct mc6470_mag_dev_s* dev, int16_t* offset)
{
  int ret;

  ret = mc6470_writereg8(dev->i2c, MC6470_MAG_ADDR, MC6470_MAG_OFFX_L, (uint8_t)(offset[0] & 0xFF));
  ret = mc6470_writereg8(dev->i2c, MC6470_MAG_ADDR, MC6470_MAG_OFFX_H, (uint8_t)(offset[0] >> 8));
  ret = mc6470_writereg8(dev->i2c, MC6470_MAG_ADDR, MC6470_MAG_OFFY_L, (uint8_t)(offset[1] & 0xFF));
  ret = mc6470_writereg8(dev->i2c, MC6470_MAG_ADDR, MC6470_MAG_OFFY_H, (uint8_t)(offset[1] >> 8));
  ret = mc6470_writereg8(dev->i2c, MC6470_MAG_ADDR, MC6470_MAG_OFFZ_L, (uint8_t)(offset[2] & 0xFF));
  ret = mc6470_writereg8(dev->i2c, MC6470_MAG_ADDR, MC6470_MAG_OFFZ_H, (uint8_t)(offset[2] >> 8));

  return ret;
}

static int mag_getoffset(struct mc6470_mag_dev_s* dev, int16_t* offset)
{
  int ret;

  ret = mc6470_readreg8(dev->i2c, MC6470_MAG_ADDR, MC6470_MAG_OFFX_L, (uint8_t*)offset, 6);

  return ret;
}

static int mag_calibrate(struct mc6470_mag_dev_s* dev)
{
  int ret;

  if (!dev->mag_state || dev->continous)
    {
      /* sensor needs to be on and in forced mode */

      return -ENODEV;
    }
  else
    {
      uint8_t regval = MC6470_MAG_CTRL3_TCS;

      /* start calibration */

      ret = mc6470_modifyreg8(dev->i2c, MC6470_MAG_ADDR,
                              MC6470_MAG_CTRL3, 0, MC6470_MAG_CTRL3_TCS);


      /* wait for calibration to complete */

      while (ret >= 0 && regval & MC6470_MAG_CTRL3_TCS)
        {
          ret = mc6470_readreg8(dev->i2c, MC6470_MAG_ADDR,
                                MC6470_MAG_CTRL3, &regval, 1);
        }

      return ret;
    }
}


/****************************************************************************
 * Name: mc6470_open
 ****************************************************************************/

static int mc6470_mag_open(FAR struct file *filep)
{
  FAR struct mc6470_mag_dev_s *priv;
  int ret = OK;

  priv = (FAR struct mc6470_mag_dev_s *)filep->f_inode->i_private;

  nxsem_wait(&priv->exclsem);

  /* Configure sensor if this is the first use */

  if (priv->open_count == 0)
    {
      ret = mag_configure(priv);
    }

  if (ret >= 0)
    {
      priv->open_count++;
    }

  nxsem_post(&priv->exclsem);

  return ret;
}

/****************************************************************************
 * Name: mc6470_close
 ****************************************************************************/

static int mc6470_mag_close(FAR struct file *filep)
{
  FAR struct mc6470_mag_dev_s *priv;
  int ret = OK;

  priv  = (FAR struct mc6470_mag_dev_s *)filep->f_inode->i_private;

  nxsem_wait(&priv->exclsem);

  priv->open_count--;

  if (priv->open_count == 0)
    {
      ret = mag_setstate(priv, false);
    }

  nxsem_post(&priv->exclsem);

  return ret;
}

/****************************************************************************
 * Name: mc6470_mag_read
 ****************************************************************************/

static ssize_t mc6470_mag_read(FAR struct file *filep, char* buffer,
                               size_t size)
{
  FAR struct mc6470_mag_dev_s *priv;
  int ret = OK;
  uint8_t status = 0;

  priv  = (FAR struct mc6470_mag_dev_s *)filep->f_inode->i_private;

  nxsem_wait(&priv->exclsem);

  /* if accelerometer is not enabled (STANDBY), can't read data */

  if (!priv->mag_state)
    {
      ret = -ENODATA;
    }
  else
    {
      if (!priv->continous)
        {
          /* in forced mode, need to force a reading */

          ret = mc6470_modifyreg8(priv->i2c, MC6470_MAG_ADDR,
                                  MC6470_MAG_CTRL3, 0, MC6470_MAG_CTRL3_FRC);

          // TODO: bit resets itself?
        }

      if (ret >= 0)
        {
          /* wait for new data to be available */

          // TODO: replace with interrupts

          while (!(status & MC6470_MAG_STAT_DRDY) && ret >= 0)
            {
              ret = mag_status(priv, &status);
            }

          if (ret >= 0)
            {
              ret = mag_read_data(priv, (int16_t*)buffer);
            }
        }
    }

  nxsem_post(&priv->exclsem);

  return ret;
}

/****************************************************************************
 * Name: mc6470_mag_ioctl
 ****************************************************************************/

static int mc6470_mag_ioctl(FAR struct file *filep, int cmd, unsigned long arg)
{
  FAR struct mc6470_mag_dev_s *priv;
  int ret = OK;

  priv  = (FAR struct mc6470_mag_dev_s *)filep->f_inode->i_private;

  nxsem_wait(&priv->exclsem);

  switch(cmd)
  {
    case SNIOC_ENABLE:
      ret = mag_setstate(priv, true);
    break;
    case SNIOC_DISABLE:
      ret = mag_setstate(priv, false);
    break;    
    case SNIOC_CALIBRATE:
      ret = mag_calibrate(priv);
    break;
    case SNIOC_SETOFFSET:
      ret = mag_setoffset(priv, (int16_t*)arg);
    break;
    case SNIOC_GETOFFSET:
      ret = mag_getoffset(priv, (int16_t*)arg);
    break;
    case SNIOC_SETMODE:
      ret = mag_setmode(priv, (bool)arg);
    break;
    default:
      ret = -EINVAL;
    break;
  }

  nxsem_post(&priv->exclsem);

  return ret;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: mc6470_mag_register
 *
 * Description:
 *   Register the MC6470 magnetometer driver as the specified device.
 *
 * Input Parameters:
 *   devname - The name of the MC6470 mag device to be registered.
 *
 * Returned Value:
 *   Zero (OK) is returned on success.  Otherwise a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 ****************************************************************************/

int mc6470_mag_register(FAR const char *devname, struct i2c_master_s* i2c)
{
  FAR struct mc6470_mag_dev_s *priv;
  int ret = OK;

  DEBUGASSERT(devname);

  /* Allocate a new mc6470 driver instance */

  priv = (FAR struct mc6470_mag_dev_s *)kmm_zalloc(sizeof(struct mc6470_mag_dev_s));

  priv->i2c = i2c;
  nxsem_init(&priv->exclsem, 0, 1);

  ret = register_driver(devname, &mc6470_mag_fops, 0666, priv);

  if (ret < 0)
  {
    kmm_free(priv);
    ret = ERROR;
  }

  return ret;
}
