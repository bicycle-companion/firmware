#!/bin/bash
export PATH=$HOME/misc/openocd/install/bin:$PATH
openocd -f interface/stlink-v2.cfg -f target/stm32l4x.cfg -c 'init; flash write_image erase nuttx/nuttx.bin 0x08000000; init; reset halt; reset run; shutdown'
