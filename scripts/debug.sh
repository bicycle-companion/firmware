#!/bin/bash
export PATH=$HOME/misc/openocd/install/bin:$PATH
openocd -f interface/stlink-v2.cfg -f target/stm32l4x.cfg -c 'set reset_config connect_assert_srst; init; reset halt'
