#!/bin/bash
FONT_PATH=$HOME/Downloads/fonts
OUTPUT_PATH=../extra_apps/bicycle_companion/fonts
TEXT_SYMBOLS=' -_.,:!?°='
ALPHANUM_RANGES='-r 48-58 -r 65-90 -r 97-122'
EXTRA_OPTS='--autohint-off'

lv_font_conv --bpp 1 --size 8 --font $FONT_PATH/nokiafc22.ttf --symbols "$TEXT_SYMBOLS" --output $OUTPUT_PATH/nokia8.c --format lvgl $EXTRA_OPTS $ALPHANUM_RANGES 

for s in 14 26 28 62; do
	lv_font_conv --bpp 1 --size $s --font $FONT_PATH/'LECO 1976 Regular.ttf' --symbols "$TEXT_SYMBOLS" --output $OUTPUT_PATH/leco$s.c --format lvgl $EXTRA_OPTS $ALPHANUM_RANGES
done

lv_font_conv --bpp 1 --size 14 --font $FONT_PATH/Koleeko.ttf --symbols "$TEXT_SYMBOLS" --output $OUTPUT_PATH/koleeko14.c --format lvgl $EXTRA_OPTS $ALPHANUM_RANGES

SYMBOL_RANGES="-r 0xf240-0xf245 -r 0xf185-0xf186"
for s in 14 16; do
        lv_font_conv --bpp 1 --size $s --font $FONT_PATH/forkawesome-webfont.ttf --output $OUTPUT_PATH/forkawesome$s.c --format lvgl $EXTRA_OPTS $SYMBOL_RANGES
done

rm -f $OUTPUT_PATH/fonts.h
for f in $OUTPUT_PATH/*.c; do
	echo $f
	sed -ri 's|#include.+|#include <nuttx/config.h>\n#include <lvgl/lvgl.h>|' $f
	sed -ri 's|lv_font_t ([^ ]+) =|lv_font_t g_font_\1 =|' $f
	font_name=$(egrep 'lv_font_t [^ ]+ =' $f | cut -f 2 -d ' ')
	echo "extern lv_font_t $font_name;" >> $OUTPUT_PATH/fonts.h 
done
