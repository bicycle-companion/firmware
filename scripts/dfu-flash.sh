#!/bin/bash
FILESIZE=$(stat --printf="%s" nuttx/nuttx.bin)
rm -f nuttx/nuttx.bin.verify

dfu-util -D nuttx/nuttx.bin -a 0 -s "0x08000000:$FILESIZE"
dfu-util -U nuttx/nuttx.bin.verify -a 0 -s "0x08000000:$FILESIZE"
if ( diff -q nuttx/nuttx.bin nuttx/nuttx.bin.verify ); then
  echo "Uploaded image OK"
  rm -rf nuttx/nuttx.bin.verify
else
  echo "Verification FAIL"
fi

