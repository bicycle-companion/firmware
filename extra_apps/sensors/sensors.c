/****************************************************************************
 * sensors.c
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <nuttx/sensors/bmp280.h>
#include <mc6470/mc6470_acc.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <nuttx/sensors/ioctl.h>
#include <math.h>

/****************************************************************************
 * Private Functions
 ****************************************************************************/

int read_baro(float* baro, float* temp)
{
  int fd;
  uint32_t data;

  /* open device */

  fd = open("/dev/press0", O_RDONLY);
  if (fd < 0)
  {
    perror("Could not open baro");
    return ERROR;
  }

  /* read pressure */

  if (read(fd, &data, sizeof(data)) != sizeof(data))
  {
    perror("Could not read pressure");
    close(fd);
    return ERROR;
  }

  *baro = data / 100.0f;

  /* read temperature */

  ioctl(fd, SNIOC_GET_TEMP, (unsigned long)&data);

  *temp = data / 100.0f;

  close(fd);

  return OK;
}

int read_acc(struct mc6470_acc_data_s* data)
{
  int fd, ret;
  struct mc6470_acc_tap_settings_s tapsettings;

  /* open IMU device */

  fd = open("/dev/acc", O_RDONLY);

  if (fd < 0)
    {
      perror("Could not open accelerometer");
      return ERROR;
    }

  /* configure tap detection */

  tapsettings.threshold[0] = 0x00;
  tapsettings.threshold[1] = 0x00;
  tapsettings.threshold[2] = 0x00;
  tapsettings.tap_detect = MC6470_TAP_BY_THRESHOLD | MC6470_TAP_X_POS |
                           MC6470_TAP_Y_POS | MC6470_TAP_Z_POS;

#if 1
  ret = ioctl(fd, SNIOC_TAP_CONFIGURE, (unsigned long)&tapsettings);
  if (ret < 0)
    {
      perror("Could not configure tap settings");
      close(fd);
      return ERROR;
    }

  ret = ioctl(fd, SNIOC_TAP_DETECT, true);
  if (ret < 0)
    {
      perror("Could not enable tap detection");
      close(fd);
      return ERROR;
    }
#endif

  /* enable accelerometer */

  ret = ioctl(fd, SNIOC_ENABLE, 0);
  if (ret < 0)
    {
      perror("Could not enable accelerometer");
      close(fd);
      return ERROR;
    }

  /* read data */

  ret = read(fd, data, sizeof(struct mc6470_acc_data_s));
  if (ret < 0)
    {
      perror("Could not read accelerometer");
      close(fd);
      return ERROR;
    }

  printf("bits: %i\n", data->tap_state);

  close(fd);

  return OK;
}

int read_mag(float* mag)
{
  int fd, ret, i;
  int16_t values[3];

  /* open IMU device */

  fd = open("/dev/mag", O_RDONLY);

  if (fd < 0)
    {
      perror("Could not open magnetometer");
      return ERROR;
    }

  /* enable magnetometer */

  ret = ioctl(fd, SNIOC_ENABLE, 0);
  if (ret < 0)
    {
      perror("Could not read magnetometer");
      close(fd);
      return ERROR;
    }

  /* calibrate */

  ret = ioctl(fd, SNIOC_CALIBRATE, 0);
  if (ret < 0)
    {
      perror("Calibrate fail");
      close(fd);
      return ERROR;
    }

  /* read data */

  ret = read(fd, &values, sizeof(values));
  if (ret < 0)
    {
      perror("Could not read magnetometer");
      close(fd);
      return ERROR;
    }

  for (i = 0; i < 3; i++)
    {
      /* 14bit (signed) assumed */

      mag[i] = (values[i] / (float)((1 << 13) - 1));
    }


  printf("Magraw:\t%i\t%i\t%i\n", values[0], values[1], values[2]);

  close(fd);

  return OK;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * sensors_main
 ****************************************************************************/

int main(int argc, char *argv[])
{
  float baro, temp, acc[3], mag[3], roll, pitch, yaw;
  int i;
  struct mc6470_acc_data_s acc_data;

  if (read_baro(&baro, &temp) < 0)
    {
      return ERROR;
    }

  if (read_acc(&acc_data) < 0)
    {
      return ERROR;
    }

  if (read_mag(mag) < 0)
    {
      return ERROR;
    }

  /* convert values */

  for (i = 0; i < 3; i++)
    {
      /* 16g, 14bit (signed) assumed */

      acc[i] = (acc_data.acc[i] / (float)((1 << 13) - 1)) * 16 * 9.8;
    }

  printf("press:\t% .2f hPa\n", baro);
  printf("temp:\t% .2f C\n", temp);

  printf("Acc:\t% .6f\t% .6f\t% .6f\n", acc[0], acc[1], acc[2]);

  printf("Taps?: %s %s %s\n",
         ((acc_data.tap_state & MC6470_TAP_X_NEG) ? "X-" : ((acc_data.tap_state & MC6470_TAP_X_POS) ? "X+" : "  ")),
         ((acc_data.tap_state & MC6470_TAP_Y_NEG) ? "Y-" : ((acc_data.tap_state & MC6470_TAP_Y_POS) ? "Y+" : "  ")),
         ((acc_data.tap_state & MC6470_TAP_Z_NEG) ? "Z-" : ((acc_data.tap_state & MC6470_TAP_Z_POS) ? "Z+" : "  ")));

  printf("Mag:\t% .6f\t% .6f\t% .6f\n", mag[0], mag[1], mag[2]);

  /* use atan2 for [-180,180] solution */
  roll = atan2(acc[1], acc[2]);

  /* use atan for [-90,90] solution */
  pitch = atanf(-acc[0] / (acc[1] * sinf(roll) + acc[2] * cosf(roll)));

  // TODO: hard-iron / soft-iron
  yaw = atan2f(
          mag[2] * sinf(roll) - mag[1] * cosf(roll),
          mag[0] * cosf(pitch) +
          mag[1] * sinf(pitch) * sinf(roll) +
          mag[2] * sinf(pitch) * cosf(roll));

  printf("rpy:\t% .6f\t% .6f\t% .6f\n", roll * 180.0f / M_PI_F, pitch * 180.0f / M_PI_F, yaw * 180.0f / M_PI_F);


  return 0;
}
