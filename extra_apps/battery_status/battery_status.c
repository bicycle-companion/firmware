/****************************************************************************
 * %{FileName}
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <sys/boardctl.h>
#include <arch/board/boardctl.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <nuttx/analog/adc.h>
#include <nuttx/analog/ioctl.h>

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * battery_status_main
 ****************************************************************************/

int main(int argc, char *argv[])
{
  uint8_t bitmask;
  int fd, ret;

  /* read battery state */

  boardctl(BOARDIOC_BATTERY_STATUS_GET, (uintptr_t)&bitmask);

  bool pg    = ((bitmask & (1 << 0)) != 0);
  bool stat1 = ((bitmask & (1 << 1)) != 0);
  bool stat2 = ((bitmask & (1 << 2)) != 0);
  printf("PG:\t%i\nSTAT1:\t%i\nSTAT2:\t%i\n", pg, stat1, stat2);

  if (bitmask == 0b110)
  {
    printf("State:\tNo battery\n");
  }
  else if (bitmask == 0b111)
  {
    printf("State:\tDischarging\n");
  }
  else if (bitmask == 0b100)
  {
    printf("State:\tCharging\n");
  }
  else if (bitmask == 0b010)
  {
    printf("State:\tCharge complete\n");
  }
  else if (bitmask == 0b000)
  {
    printf("State:\tTemperature/timer fault\n");
  }
  else if (bitmask == 0b101)
  {
    printf("State:\tBattery critically low\n");
  }
  else
  {
    printf("State:\tUnknown\n");
  }

  /* open ADC */

  fd = open("/dev/batt", O_RDONLY);
  if (fd < 0)
  {
    perror("Could not open ADC");
    return ERROR;
  }

  /* read ADC */

  {
    struct adc_msg_s adcm;

    ioctl(fd, ANIOC_TRIGGER, 0);

    ret = read(fd, &adcm, sizeof(adcm));
    if (ret != sizeof(adcm))
    {
      perror("Could not read ADC");
      return ERROR;
    }

    /* compute battery voltage assuming a VCC = 3.26v */

    printf("BAT:\t%.3f v\n", (adcm.am_data / 4095.0) * 3.26 * 2);
  }

  if (fd > 0)
  {
    close(fd);
  }

  return 0;
}
