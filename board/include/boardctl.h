/****************************************************************************
 * boards/arm/stm32l4/nucleo-l476rg/include/board.h
 *
 *   Copyright (C) 2016, 2018-2019 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef __BOARDS_ARM_STM32L4_NUCLEO_L476RG_INCLUDE_BOARDCTL_H
#define __BOARDS_ARM_STM32L4_NUCLEO_L476RG_INCLUDE_BOARDCTL_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#ifndef __ASSEMBLY__
# include <stdint.h>
#endif
#include <sys/boardctl.h>

/* Do not include STM32 L4 header files here */

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define BOARDIOC_PULSECOUNTER_GET     (BOARDIOC_USER + 0) /* arg = uint32_t* : pointer to counter value */
#define BOARDIOC_MOTOR_SET            (BOARDIOC_USER + 1) /* arg = int : on or off */
#define BOARDIOC_BATTERY_STATUS_GET   (BOARDIOC_USER + 2) /* arg = uint8_t* : pointer to bitmaks: PG, STAT1, STAT2 */
#define BOARDIOC_SETSTANDBYMODE       (BOARDIOC_USER + 3) /* arg = uint8_t: 1 or 2 for STOP1 or STOP2 */
#define BOARDIOC_GETSTANDBYMODE       (BOARDIOC_USER + 4) /* arg = *uint8t_t: 1 or 2 for STOP1 or STOP2 */
#define BOARDIOC_PULSECOUNTER_NOTIFY  (BOARDIOC_USER + 5) /* arg = struct sigevent* */

#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */
#endif /* __BOARDS_ARM_STM32L4_NUCLEO_L476RG_INCLUDE_BOARDCTL_H */
