/****************************************************************************
 * boards/arm/stm32l4/nucleo-l476rg/src/nucleo-l476rg.h
 *
 *   Copyright (C) 2014, 2016, 2018-2019 Gregory Nutt. All rights reserved.
 *   Authors: Frank Bennett
 *            Gregory Nutt <gnutt@nuttx.org>
 *            Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef __BOARDS_ARM_STM32L4_NUCLEO_L476RG_SRC_NUCLEO_L476RG_H
#define __BOARDS_ARM_STM32L4_NUCLEO_L476RG_SRC_NUCLEO_L476RG_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/compiler.h>

#include <stdint.h>

#include "stm32l4_gpio.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Configuration ************************************************************/

#define HAVE_PROC           1

#if !defined(CONFIG_FS_PROCFS)
#  undef HAVE_PROC
#endif

#if defined(HAVE_PROC) && defined(CONFIG_DISABLE_MOUNTPOINT)
#  warning Mountpoints disabled.  No procfs support
#  undef HAVE_PROC
#endif

/*
 * LEDs
 */

#define GPIO_LD1 \
  (GPIO_PORTC | GPIO_PIN10 | GPIO_OUTPUT_CLEAR | GPIO_OUTPUT | GPIO_SPEED_50MHz)
#define GPIO_LD2 \
  (GPIO_PORTC | GPIO_PIN11 | GPIO_OUTPUT_CLEAR | GPIO_OUTPUT | GPIO_SPEED_50MHz)
#define GPIO_LD3 \
  (GPIO_PORTC | GPIO_PIN12 | GPIO_OUTPUT_CLEAR | GPIO_OUTPUT | GPIO_SPEED_50MHz)

/*
 * Buttons
 */

#define MIN_IRQBUTTON   GPIO_BTN_BL
#define MAX_IRQBUTTON   GPIO_BTN_UR
#define NUM_IRQBUTTONS  4

#define GPIO_BTN_BL \
  (GPIO_INPUT |GPIO_PULLUP |GPIO_EXTI | GPIO_PORTH | GPIO_PIN0)
#define GPIO_BTN_UL \
  (GPIO_INPUT |GPIO_PULLUP |GPIO_EXTI | GPIO_PORTC | GPIO_PIN13)
#define GPIO_BTN_BR \
  (GPIO_INPUT |GPIO_PULLUP |GPIO_EXTI | GPIO_PORTC | GPIO_PIN4)
#define GPIO_BTN_UR \
  (GPIO_INPUT |GPIO_PULLUP |GPIO_EXTI | GPIO_PORTD | GPIO_PIN2)

/* SPI chip selects */

#ifdef CONFIG_LCD_SHARP_MEMLCD
#define GPIO_MEMLCD_DISP     (GPIO_PORTA | GPIO_PIN6 | GPIO_OUTPUT_CLEAR | \
                              GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz)

#define GPIO_MEMLCD_CS       (GPIO_PORTA | GPIO_PIN4 | GPIO_OUTPUT_CLEAR | \
                              GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz)
#endif

/*
 * Other GPIOs
 */

#define GPIO_MOTOR \
  (GPIO_PORTC | GPIO_PIN9 | GPIO_OUTPUT_CLEAR | GPIO_OUTPUT | GPIO_SPEED_50MHz)

#define GPIO_BAT_PG \
  (GPIO_PORTA | GPIO_PIN1 | GPIO_INPUT | GPIO_PULLUP)
#define GPIO_BAT_STAT1 \
  (GPIO_PORTA | GPIO_PIN3 | GPIO_INPUT | GPIO_PULLUP)
#define GPIO_BAT_STAT2 \
  (GPIO_PORTA | GPIO_PIN2 | GPIO_INPUT | GPIO_PULLUP)

#define GPIO_MC6470_ACC_INT \
  (GPIO_PORTA | GPIO_PIN0 | GPIO_INPUT | GPIO_PULLUP | GPIO_EXTI)

#define GPIO_OTGFS_VBUS   (GPIO_INPUT|GPIO_FLOAT|GPIO_SPEED_100MHz|\
                           GPIO_OPENDRAIN|GPIO_PORTA|GPIO_PIN9)


#define STANDBYMODE_STOP1 1
#define STANDBYMODE_STOP2 2

/****************************************************************************
 * Public Data
 ****************************************************************************/

/* Global driver instances */

#ifdef CONFIG_STM32L4_SPI1
extern struct spi_dev_s *g_spi1;
#endif

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: stm32l4_spiinitialize
 *
 * Description:
 *   Called to configure SPI chip select GPIO pins.
 *
 ****************************************************************************/

void stm32l4_spiinitialize(void);

/****************************************************************************
 * Name: stm32l4_usbinitialize
 *
 * Description:
 *   Called to setup USB-related GPIO pins.
 *
 ****************************************************************************/

void stm32l4_usbinitialize(void);

/****************************************************************************
 * Name: stm32l4_pwm_setup
 *
 * Description:
 *   Initialize PWM and register the PWM device.
 *
 ****************************************************************************/

#ifdef CONFIG_PWM
int stm32l4_pwm_setup(void);
#endif

/****************************************************************************
 * Name: stm32l4_adc_setup
 *
 * Description:
 *   Initialize ADC and register the ADC driver.
 *
 ****************************************************************************/

#ifdef CONFIG_ADC
int stm32l4_adc_setup(void);
#endif

/****************************************************************************
 * Name: board_timer_driver_initialize
 *
 * Description:
 *   Initialize and register a timer
 *
 ****************************************************************************/

#ifdef CONFIG_TIMER
int board_timer_driver_initialize(FAR const char *devpath, int timer);
#endif

/****************************************************************************
 * Name: stm32_bmp280initialize
 *
 * Description:
 * Called to configure an I2C and to register BMP180.
 *
 ****************************************************************************/

#ifdef CONFIG_SENSORS_BMP280
int stm32_bmp280initialize(FAR const char *devpath);
#endif

void stm32l4_pulsecounter_initialize(void);
uint32_t stm32l4_pulsecounter_getcounter(void);

struct sigevent;
void stm32l4_pulsecounter_notify(struct sigevent* evt);

void board_composite_disconnect(void);

void stm32l4_setstandbymode(int mode);
int stm32l4_getstandbymode(void);

#endif /* __BOARDS_ARM_STM32L4_NUCLEO_L476RG_SRC_NUCLEO_L476RG_H */
