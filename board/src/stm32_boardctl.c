/****************************************************************************
 * boards/arm/stm32l4/nucleo-l476rg/src/stm32_pulsecounter.c
 *
 *   Copyright (C) 2011 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *   Modified: Librae <librae8226@gmail.com>
 *   Modified: Matias Nitsche <mnitsche@dc.ub.ar>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/arch.h>
#include <nuttx/board.h>
#include <arch/board/boardctl.h>
#include "stm32l4_gpio.h"

#include "bicycle-companion.h"

/****************************************************************************
 * Private Data
 ***************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

#ifdef CONFIG_BOARDCTL_IOCTL
int board_ioctl(unsigned int cmd, uintptr_t arg)
{
  switch(cmd)
    {
      case BOARDIOC_PULSECOUNTER_GET:
        *((uint32_t*)arg) = stm32l4_pulsecounter_getcounter();
      break;
      case BOARDIOC_PULSECOUNTER_NOTIFY:
        stm32l4_pulsecounter_notify((struct sigevent*)arg);
      break;
      case BOARDIOC_MOTOR_SET:
        stm32l4_gpiowrite(GPIO_MOTOR, arg);
      break;
      case BOARDIOC_BATTERY_STATUS_GET:
        *(uint8_t*)arg = (stm32l4_gpioread(GPIO_BAT_PG)    << 0) |
                         (stm32l4_gpioread(GPIO_BAT_STAT1) << 1) |
                         (stm32l4_gpioread(GPIO_BAT_STAT2) << 2);
      break;
      case BOARDIOC_SETSTANDBYMODE:
        stm32l4_setstandbymode(arg);
      break;
      case BOARDIOC_GETSTANDBYMODE:
        *(uint8_t*)arg = stm32l4_getstandbymode();
      break;
    }

  return OK;
}
#endif
