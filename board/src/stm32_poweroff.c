/****************************************************************************
 * boards/arm/stm32/stm32f4discovery/src/stm32_reset.c
 *
 *   Copyright (C) 2019 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <nuttx/arch.h>
#include <nuttx/board.h>
#include <nuttx/power/pm.h>

#include "arm_arch.h"
#include "bicycle-companion.h"
#include "stm32l4_pm.h"
#include "stm32l4_pwr.h"

#ifdef CONFIG_BOARDCTL_POWEROFF

#define PM_IDLE_DOMAIN 0

/****************************************************************************
 * Public functions
 ****************************************************************************/

/****************************************************************************
 * Name: board_power_off
 ****************************************************************************/

int board_power_off(int status)
{
  uint32_t regval;

  /* wakeup on select pins */

  regval  = getreg32(STM32L4_PWR_CR3);
  regval |= PWR_CR3_APC | PWR_CR3_EWUP1 | PWR_CR3_EWUP2 | PWR_CR3_EWUP5;
  putreg32(regval, STM32L4_PWR_CR3);

  /* wakeup on falling edge of pin signals */

  regval  = getreg32(STM32L4_PWR_CR4);
  regval |= PWR_CR4_WP1 | PWR_CR4_WP2 | PWR_CR4_WP5;
  putreg32(regval, STM32L4_PWR_CR4);

  /* set wake pins as pull-ups from here */

  putreg32((1 << 13) | (1 << 5), STM32L4_PWR_PUCRC);  /* PC13 and PC5 */
  putreg32((1 << 0), STM32L4_PWR_PUCRA);              /* PA0 */

  /* set LCD EN/CS pins as pull-down to avoid ghost wakeup */

  putreg32((1 << 4) | (1 << 6), STM32L4_PWR_PDCRA);  /* PA6 and PA4 */

  /* set LED pins as pulldown */

  putreg32((1 << 10) | (1 << 11) | (1 << 12), STM32L4_PWR_PDCRC);  /* PC10-12 */

  /* go away */

  stm32l4_pmshutdown();

  return 0;
}

#endif /* CONFIG_BOARDCTL_POWEROFF */
