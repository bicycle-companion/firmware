/****************************************************************************
 * boards/arm/stm32l4/nucleo-l476rg/src/stm32_pulsecounter.c
 *
 *   Copyright (C) 2011 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *   Modified: Librae <librae8226@gmail.com>
 *   Modified: Matias Nitsche <mnitsche@dc.ub.ar>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/arch.h>
#include <nuttx/board.h>
#include <nuttx/spi/spi.h>
#include <nuttx/lcd/lcd.h>
#include <nuttx/lcd/memlcd.h>
#include <nuttx/timers/pwm.h>
#include <nuttx/power/pm.h>

#include "stm32l4_gpio.h"
#include "stm32l4_lptim.h"
#include "bicycle-companion.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
/* Configuration ************************************************************/

#define STM32L4_GPIO_REED_LPTIM           (GPIO_LPTIM2_IN1_2 | GPIO_PULLUP)
#define STM32L4_GPIO_REED_LPTIM_DISABLED  (GPIO_PORTC | GPIO_PIN0 | GPIO_INPUT | GPIO_FLOAT)
#define STM32L4_GPIO_REED_WKUP            (GPIO_PORTC | GPIO_PIN5 | GPIO_INPUT | GPIO_PULLUP | GPIO_EXTI)

/****************************************************************************
 * Private Types
 ****************************************************************************/

struct pulsecounter_notify_s
{
  struct sigevent evt;
  pid_t pid;
};

/****************************************************************************
 * Private Function Declarations
 ****************************************************************************/

static int pm_prepare(FAR struct pm_callback_s *cb, int domain,
                      enum pm_state_e pmstate);

static void pm_notify(FAR struct pm_callback_s *cb, int domain,
                      enum pm_state_e pmstate);

static void stm32l4_pulsecounter_config(bool counter_mode);

static int reed_isr(int irq, FAR void *context, FAR void *arg);

/****************************************************************************
 * Private Data
 ****************************************************************************/

struct stm32l4_lptim_dev_s* lptim2 = NULL;
struct pm_callback_s g_pm_callbacks =
{
  .prepare = pm_prepare,
  .notify =  pm_notify
};

struct pulsecounter_notify_s g_notify =
{
  .pid = 0
};

uint32_t g_counter = 0;

/****************************************************************************
 * Private Functions
 ****************************************************************************/

static int pm_prepare(FAR struct pm_callback_s *cb, int domain,
                      enum pm_state_e pmstate)
{
  /* accept all PM state changes */

  return 0;
}

static void pm_notify(FAR struct pm_callback_s *cb, int domain,
                      enum pm_state_e pmstate)
{
  if (stm32l4_getstandbymode() == STANDBYMODE_STOP2)
  {
    if (pmstate == PM_STANDBY || pmstate == PM_SLEEP)
    {
      /* We're about to enter STOP2 or SHUTDOWN mode,
       * LPTIM needs to be deactivated and the pin set as interrupt line
       * so we wake up from a pulse there */

      stm32l4_pulsecounter_config(false);
    }
  }
  else
  {
    if (pmstate == PM_NORMAL && !lptim2)
    {
      /* We're returning from STOP2 mode since LPTIM is disabled
       * and an interrupt from ISR woke us up, so re-enable LPTIM */

      stm32l4_pulsecounter_config(true);
    }
  }
}

static int reed_isr(int irq, FAR void *context, FAR void *arg)
{
  /* If we're waking up due to this interrupt, we should switch
   * request to use STOP1 again. We take care not to do this twice
   * to avoid sending more than one notification in case of signal
   * glitches
   */

  if (stm32l4_getstandbymode() == STANDBYMODE_STOP2)
  {
    stm32l4_setstandbymode(STANDBYMODE_STOP1);

    /* this ISR was due to a count */

    g_counter++;

    if (g_notify.pid)
    {
      /* send a signal to the process */

      nxsig_notification(g_notify.pid, &g_notify.evt, SI_QUEUE, NULL);
    }
  }
  return 0;
}

static void stm32l4_pulsecounter_config(bool counter_mode)
{
  if (counter_mode && !lptim2)
  {
    /* set LPTIM pin, unconfig WKUP pin */

    stm32l4_gpiosetevent(STM32L4_GPIO_REED_WKUP, false, true, false,
                         NULL, NULL);
    stm32l4_unconfiggpio(STM32L4_GPIO_REED_WKUP);

    stm32l4_configgpio(STM32L4_GPIO_REED_LPTIM);

    /* setup LPTIM2 for counting, which works in STOP1 mode */

    lptim2 = stm32l4_lptim_init(2);

    STM32L4_LPTIM_SETCLOCKSOURCE(lptim2, STM32L4_LPTIM_CLK_EXT);
    STM32L4_LPTIM_SETMODE(lptim2, STM32L4_LPTIM_MODE_CONTINUOUS);
    STM32L4_LPTIM_SETPERIOD(lptim2, 0xffff);
  }
  else if (!counter_mode && lptim2)
  {
    /* remember current count, since we will loose this now */

    g_counter = STM32L4_LPTIM_GETCOUNTER(lptim2);

    /* setup pin for interrupt to wakeup from STOP2 mode */

    stm32l4_lptim_deinit(lptim2);
    lptim2 = NULL;

    /* unconfig LPTIM pin and set WKUP pin to EXTI */

    stm32l4_unconfiggpio(STM32L4_GPIO_REED_LPTIM);
    stm32l4_gpiosetevent(STM32L4_GPIO_REED_WKUP, false, true, false,
                         reed_isr, NULL);
  }
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

void stm32l4_pulsecounter_initialize(void)
{
  stm32l4_configgpio(STM32L4_GPIO_REED_LPTIM_DISABLED);

  stm32l4_gpiosetevent(STM32L4_GPIO_REED_WKUP, false, true, false,
                       reed_isr, NULL);

  pm_register(&g_pm_callbacks);
}

uint32_t stm32l4_pulsecounter_getcounter(void)
{
  if (lptim2)
  {
    return g_counter + STM32L4_LPTIM_GETCOUNTER(lptim2);
  }
  else
  {
    return g_counter;
  }
}

void stm32l4_pulsecounter_notify(struct sigevent* evt)
{
  g_notify.pid = getpid();
  g_notify.evt = *evt;
}

